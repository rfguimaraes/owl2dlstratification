/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * The axioms are compared according to specificity or generality of the concepts in their
 * signatures.
 */
public abstract class ConceptNameInducedStrictPoset implements StrictPoset<OWLAxiom> {

  protected final OWLOntology ontology;
  protected final Map<OWLClass, Long> conceptRanks;
  protected Graph<OWLAxiom> relationGraph;
  protected final OWLReasoner reasoner;

  /**
   * @param ontology input ontology.
   * @param conceptRanks the concept ranking to use.
   * @param reasoner a reasoner for the input ontology.
   */
  protected ConceptNameInducedStrictPoset(OWLOntology ontology,
      Map<OWLClass, Long> conceptRanks, OWLReasoner reasoner) {
    this.ontology = ontology;
    this.conceptRanks = conceptRanks;
    this.reasoner = reasoner;
    createGraph();
  }

  private void createGraph() {
    MutableGraph<OWLAxiom> tmpGraph = GraphBuilder.directed().allowsSelfLoops(false).build();
    ontology.logicalAxioms().forEach(axiom -> addAxiom(tmpGraph, axiom));
    relationGraph = ImmutableGraph.copyOf(tmpGraph);
  }

  private void addAxiom(MutableGraph<OWLAxiom> graph, OWLAxiom axiom) {
    if (!inDomain(axiom)) {
      return;
    }

    graph.addNode(axiom);

    for (OWLAxiom otherAxiom : graph.nodes()) {

      if (prec(axiom, otherAxiom).orElse(false)) {
        graph.putEdge(otherAxiom, axiom);
      }

      if (prec(otherAxiom, axiom).orElse(false)) {
        graph.putEdge(axiom, otherAxiom);
      }
    }
  }

  public long getRank(OWLAxiom axiom) {
    return getActualClassesInSignature(axiom).map(conceptRanks::get).reduce(Long::min)
        .orElse((long) -1);
  }

  protected Set<OWLClass> getMinConcepts(OWLAxiom axiom) {
    long minRank = getRank(axiom);
    return getActualClassesInSignature(axiom)
        .filter(concept -> conceptRanks.get(concept) == minRank).collect(Collectors.toSet());
  }

  /**
   * @return the actual classes considered from the axiom's signature.
   */
  protected Stream<OWLClass> getActualClassesInSignature(OWLAxiom axiom) {

    return axiom.classesInSignature()
        .filter(owlClass -> !(owlClass.isTopEntity() || owlClass.isBottomEntity()));

  }

  /**
   * @return the axioms which have minimal concepts in their signatures.
   */
  @Override
  public Set<OWLAxiom> getMinimal() {
    Optional<Long> optMinRank = ontology.logicalAxioms().map(this::getRank)
        .filter(l -> !l.equals((long) -1)).reduce(Long::min);
    if (optMinRank.isPresent()) {
      return ontology.logicalAxioms().filter(axiom -> getRank(axiom) == optMinRank.get())
          .collect(Collectors.toSet());
    }
    return new HashSet<>();
  }

  @Override
  public Set<OWLAxiom> getSuccessors(OWLAxiom axiom) {
    return relationGraph.predecessors(axiom);
  }

  @Override
  public boolean inDomain(OWLAxiom axiom) {
    return getActualClassesInSignature(axiom).count() > 0;
  }
}
