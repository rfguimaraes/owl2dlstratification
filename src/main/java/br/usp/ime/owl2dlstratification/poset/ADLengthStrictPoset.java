/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import uk.ac.manchester.cs.atomicdecomposition.Atom;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;

public abstract class ADLengthStrictPoset implements StrictPoset<OWLAxiom> {

  protected final AtomicDecomposition atomicDecomposition;

  protected ADLengthStrictPoset(AtomicDecomposition atomicDecomposition) {
    this.atomicDecomposition = atomicDecomposition;
  }

  /**
   * Compares two atoms regarding their dependency relation in the atomic decomposition.
   *
   * @param lowerIsBetter true if lower height is better, false otherwise.
   * @return whether the first atom is better, worse, equal or incomparable to the second.
   */
  static boolean prec(Atom atomForAxiom, Atom atomForOtherAxiom,
      AtomicDecomposition atomicDecomposition, boolean lowerIsBetter) {
    if (atomForAxiom.equals(atomForOtherAxiom)) {
      return false;
    }
    return (lowerIsBetter && atomicDecomposition.getDependencies(atomForOtherAxiom)
        .contains(atomForAxiom)) || (!lowerIsBetter && atomicDecomposition
        .getDependents(atomForOtherAxiom).contains(atomForAxiom));
  }

  abstract Set<Atom> getMinimalAtoms();

  abstract Set<Atom> getSuccessorAtoms(Atom atom);

  /**
   * @return The set of axioms in the minimal atoms.
   */
  @Override
  public Set<OWLAxiom> getMinimal() {
    return getMinimalAtoms().stream().flatMap(
        atom -> atom.getAxioms().stream().filter(OWLAxiom::isLogicalAxiom)
            .map(axiom -> (OWLLogicalAxiom) axiom)).collect(Collectors.toSet());
  }

  /**
   * @param axiom the axiom in question.
   * @return returns the axioms which are in atoms succeeding the atom for the axiom.
   */
  @Override
  public Set<OWLAxiom> getSuccessors(OWLAxiom axiom) {
    Atom atomForAxiom = atomicDecomposition.getAtomForAxiom(axiom);
    return getSuccessorAtoms(atomForAxiom).stream().flatMap(
        atom -> atom.getAxioms().stream().filter(OWLAxiom::isLogicalAxiom)
            .map(ax -> (OWLAxiom) ax)).collect(Collectors.toSet());
  }

  /**
   * @return whether the axiom is part of the atomic decomposition.
   */
  @Override
  public boolean inDomain(OWLAxiom axiom) {
    return atomicDecomposition.getAtomForAxiom(axiom) != null;
  }
}
