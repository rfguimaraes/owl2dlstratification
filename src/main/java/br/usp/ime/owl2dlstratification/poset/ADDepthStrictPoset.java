/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import java.util.Optional;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;
import uk.ac.manchester.cs.atomicdecomposition.Atom;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;

public class ADDepthStrictPoset extends ADLengthStrictPoset {

  public ADDepthStrictPoset(AtomicDecomposition atomicDecomposition) {
    super(atomicDecomposition);
  }

  @Override
  Set<Atom> getMinimalAtoms() {
    return atomicDecomposition.getTopAtoms();
  }

  @Override
  Set<Atom> getSuccessorAtoms(Atom atom) {
    Set<Atom> successors = atomicDecomposition.getDependencies(atom, false);
    successors.remove(atom);
    return successors;
  }

  @Override
  public Optional<Boolean> prec(OWLAxiom axiom, OWLAxiom otherAxiom) {

    if (!inDomain(axiom) || !inDomain(otherAxiom)) {
      return Optional.empty();
    }

    Atom atomForAxiom = atomicDecomposition.getAtomForAxiom(axiom);
    Atom atomForOtherAxiom = atomicDecomposition.getAtomForAxiom(otherAxiom);

    return Optional.of(prec(atomForAxiom, atomForOtherAxiom, atomicDecomposition, false));
  }
}
