/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import com.google.common.collect.Sets;
import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import com.google.common.graph.MutableGraph;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLNaryClassAxiom;
import org.semanticweb.owlapi.model.OWLNaryPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChange;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiomShortCut;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLEquivalentClassesAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLObjectIntersectionOfImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLSubClassOfAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLSubDataPropertyOfAxiomImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLSubObjectPropertyOfAxiomImpl;

public class AxiomSpecificityStrictPoset implements StrictPoset<OWLAxiom> {

  protected final OWLOntology ontology;
  private SpecificityComparator specificityComparator;

  protected Graph<OWLAxiom> relationGraph;

  public AxiomSpecificityStrictPoset(OWLOntologyManager manager, OWLOntology ontology,
      OWLReasonerFactory reasonerFactory,
      OWLReasonerConfiguration reasonerConfiguration) throws OWLOntologyCreationException {
    this.ontology = ontology;
    this.specificityComparator = new SpecificityComparator(manager, ontology, reasonerFactory,
        reasonerConfiguration);
    buildGraph();
  }

  private void buildGraph() {
    MutableGraph<OWLAxiom> tmpGraph = GraphBuilder.directed().allowsSelfLoops(false).build();
    ontology.logicalAxioms().forEach(axiom -> addAxiom(tmpGraph, axiom));
    relationGraph = ImmutableGraph.copyOf(tmpGraph);
  }

  private void addAxiom(MutableGraph<OWLAxiom> graph, OWLAxiom axiom) {

    if (!inDomain(axiom)) {
      return;
    }

    graph.addNode(axiom);

    for (OWLAxiom otherAxiom : graph.nodes()) {

      if (prec(axiom, otherAxiom).orElse(false)) {
        graph.putEdge(otherAxiom, axiom);
      }

      if (prec(otherAxiom, axiom).orElse(false)) {
        graph.putEdge(axiom, otherAxiom);
      }
    }
  }

  @Override
  public Optional<Boolean> prec(OWLAxiom axiom, OWLAxiom otherAxiom) {
    if (!inDomain(axiom) || !inDomain(otherAxiom)) {
      return Optional.empty();
    }

    if (!axiom.isLogicalAxiom() || !otherAxiom.isLogicalAxiom()) {
      return Optional.empty();
    }
    return Optional.of(this.specificityComparator.compare((OWLLogicalAxiom) axiom, (OWLLogicalAxiom) otherAxiom));
  }

  @Override
  public Set<OWLAxiom> getMinimal() {
    return relationGraph.nodes().stream().filter(node -> relationGraph.outDegree(node) == 0)
        .collect(Collectors.toSet());
  }

  @Override
  public Set<OWLAxiom> getSuccessors(OWLAxiom axiom) {
    return relationGraph.predecessors(axiom);
  }

  /**
   * @return whether the axiom is a logical axiom and not a SWRL rule
   */
  @Override
  public boolean inDomain(OWLAxiom axiom) {
    return !axiom.isOfType(AxiomType.SWRL_RULE);
  }

  private static class SpecificityComparator {

    private final OWLOntology auxOntology;
    private final OWLReasoner reasoner;
    private final Set<AxiomType<?>> rBoxAxioms;
    private final Set<AxiomType<?>> aBoxAxioms;
    private final Set<AxiomType<?>> maximalCBoxAxioms;
    private final Set<AxiomType<?>> middleCBoxAxioms;
    private final Set<AxiomType<?>> dBoxAxioms;
    private final Set<AxiomType<?>> pBoxAxioms;
    private final OWLOntologyManager manager;
    private final Map<OWLAxiom, OWLClassExpression> auxMap;


    public SpecificityComparator(OWLOntologyManager manager, OWLOntology ontology,
        OWLReasonerFactory reasonerFactory, OWLReasonerConfiguration configuration)
        throws OWLOntologyCreationException {
      this.manager = manager;

      // The most specific axiom types
      aBoxAxioms = Sets.newHashSet(AxiomType.ABoxAxiomTypes);
      aBoxAxioms.add(AxiomType.HAS_KEY);

      // Axioms that establish relations between roles
      rBoxAxioms = Sets.newHashSet(
          AxiomType.SUB_OBJECT_PROPERTY,
          AxiomType.SUB_PROPERTY_CHAIN_OF,
          AxiomType.TRANSITIVE_OBJECT_PROPERTY,
          // The next ones should have the same treatment
          AxiomType.EQUIVALENT_OBJECT_PROPERTIES, // OWLNaryPropertyAxiom
          AxiomType.INVERSE_OBJECT_PROPERTIES, // OWLNaryPropertyAxiom
          AxiomType.DISJOINT_OBJECT_PROPERTIES, // OWLNaryPropertyAxiom
          // The next ones should have the same treatment
          AxiomType.ASYMMETRIC_OBJECT_PROPERTY,
          AxiomType.SYMMETRIC_OBJECT_PROPERTY
      );

      // Axioms that establish relations between concepts
      middleCBoxAxioms = Sets.newHashSet(
          AxiomType.SUBCLASS_OF,
          AxiomType.OBJECT_PROPERTY_DOMAIN, // SubClassOfShortCut
          AxiomType.EQUIVALENT_CLASSES, // List of CEs (OWLNAryClassAxiom)
          AxiomType.DISJOINT_CLASSES, // List of CEs (OWLNAryClassAxiom)
          AxiomType.DISJOINT_UNION, // Get the "Disjoint part"

          AxiomType.DATA_PROPERTY_DOMAIN // SubClassOfShortCut
      );

      // Axioms that establish relations between concepts, but that are always the most general
      maximalCBoxAxioms = Sets
          .newHashSet(AxiomType.FUNCTIONAL_OBJECT_PROPERTY); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.REFLEXIVE_OBJECT_PROPERTY); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.IRREFLEXIVE_OBJECT_PROPERTY); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.DATA_PROPERTY_RANGE); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.OBJECT_PROPERTY_RANGE); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.FUNCTIONAL_DATA_PROPERTY); // SubClassOfShortCut
      maximalCBoxAxioms.add(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY); // SubClassOfShortCut

      // Axioms that establish relations between datatypes
      dBoxAxioms = Sets.newHashSet(
          //AxiomType.DATA_PROPERTY_DOMAIN, // SubClassOfShortCut
          AxiomType.DATATYPE_DEFINITION
      );

      // Axioms that establish relations between data properties
      pBoxAxioms = Sets.newHashSet(
          AxiomType.SUB_DATA_PROPERTY,
          AxiomType.EQUIVALENT_DATA_PROPERTIES, // OWLNaryPropertyAxiom
          AxiomType.DISJOINT_DATA_PROPERTIES // OWLNaryPropertyAxiom
      );

      /* Optimization suggested by Bijan Parsia: create an auxiliar ontology with new axioms for the
         LHS of some axioms and make use of the already existing classification techniques to ease
         the computation of specificity for most axioms.
      */
      auxOntology = manager.createOntology(ontology.axioms());
      auxMap = new HashMap<>();
      ontology.logicalAxioms()
          .filter(axiom -> axiom.isOfType(middleCBoxAxioms) || axiom.isOfType(
              maximalCBoxAxioms))
          .map(
              this::newAxiom)
          .forEach(auxOntology::applyChange);

      this.reasoner = reasonerFactory.createNonBufferingReasoner(auxOntology, configuration);

      reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
      reasoner.precomputeInferences(InferenceType.OBJECT_PROPERTY_HIERARCHY);
      reasoner.precomputeInferences(InferenceType.DATA_PROPERTY_HIERARCHY);
      reasoner.precomputeInferences(InferenceType.DISJOINT_CLASSES);

    }

    /* Create a new axiom as classification-shorthand for the LHS of an existing one */
    private OWLOntologyChange newAxiom(OWLLogicalAxiom axiom) {
      String uriString = auxOntology.getOntologyID().getOntologyIRI().toString();
      uriString = uriString + "#__fresh_axiom_number__" + String.valueOf(auxMap.size());
      OWLClass freshClass = manager.getOWLDataFactory().getOWLClass(uriString);

      Set<OWLClassExpression> classExpressions = getRelevantClassExpressions(axiom);
      OWLEquivalentClassesAxiom newAxiom;
      if (axiom.isOfType(AxiomType.DISJOINT_CLASSES) || axiom.isOfType(AxiomType.DISJOINT_UNION)) {
        OWLObjectIntersectionOfImpl intersection = new OWLObjectIntersectionOfImpl(
            classExpressions);
        newAxiom = new OWLEquivalentClassesAxiomImpl(Sets.newHashSet(intersection, freshClass),
            Sets.newHashSet());
      } else if (axiom.isOfType(AxiomType.EQUIVALENT_CLASSES)) {
        classExpressions.add(freshClass);
        newAxiom = new OWLEquivalentClassesAxiomImpl(classExpressions, Sets.newHashSet());
      } else {
        OWLClassExpression arg = classExpressions.iterator().next();
        newAxiom = new OWLEquivalentClassesAxiomImpl(Sets.newHashSet(arg, freshClass),
            Sets.newHashSet());
      }

      OWLOntologyChange owlOntologyChange = new AddAxiom(auxOntology, newAxiom);
      auxMap.put(axiom, freshClass);
      return owlOntologyChange;
    }

    public boolean compare(OWLLogicalAxiom axiom1, OWLLogicalAxiom axiom2) {

      // Easy-to-solve comparisons
      if (axiom1.isOfType(aBoxAxioms)) {
        if (axiom2.isOfType(aBoxAxioms)) {
          return false;
        } else {
          return true;
        }
      } else if (axiom2.isOfType(aBoxAxioms)) {
        return false;
      } else if (axiom1.isOfType(maximalCBoxAxioms)) {
        if (axiom2.isOfType(maximalCBoxAxioms)) {
          return false;
        }
      }

      if (axiom1.isOfType(middleCBoxAxioms) && axiom2.isOfType(middleCBoxAxioms)) {
        return compare(auxMap.get(axiom1), auxMap.get(axiom2));
      } else if (axiom1.isOfType(middleCBoxAxioms) && axiom2.isOfType(maximalCBoxAxioms)
          || axiom1.isOfType(maximalCBoxAxioms) && axiom2.isOfType(middleCBoxAxioms)) {
        return compare(auxMap.get(axiom1), auxMap.get(axiom2));
      } else if (axiom1.isOfType(rBoxAxioms) && axiom2.isOfType(rBoxAxioms)) {
        return compareObjectProperties(getRelevantObjectPropertyExpressions(axiom1),
            getRelevantObjectPropertyExpressions(axiom2));
      } else if (axiom1.isOfType(dBoxAxioms) && axiom2.isOfType(dBoxAxioms)) {
        return compareDatatypes(axiom1, axiom2);
      } else if (axiom1.isOfType(pBoxAxioms) && axiom2.isOfType(pBoxAxioms)) {
        return compareDataProperties(getRelevantDataPropertyExpressions(axiom1),
            getRelevantDataPropertyExpressions(axiom2));
      }
      return false;
    }

    private Set<OWLClassExpression> getRelevantClassExpressions(OWLLogicalAxiom axiom) {
      if (axiom.isOfType(AxiomType.SUBCLASS_OF)) {
        return getRelevantClassExpressions((OWLSubClassOfAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.OBJECT_PROPERTY_DOMAIN, AxiomType.DATA_PROPERTY_DOMAIN) ||
          axiom.isOfType(maximalCBoxAxioms)) {
        return getRelevantClassExpressions((OWLSubClassOfAxiomShortCut) axiom);
      } else if (axiom.isOfType(AxiomType.EQUIVALENT_CLASSES, AxiomType.DISJOINT_CLASSES)) {
        return getRelevantClassExpressions((OWLNaryClassAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.DISJOINT_UNION)) {
        return getRelevantClassExpressions((OWLDisjointUnionAxiom) axiom);
      }
      return Collections.emptySet();
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLLogicalAxiom axiom) {
      if (axiom.isOfType(AxiomType.SUB_OBJECT_PROPERTY)) {
        return getRelevantObjectPropertyExpressions((OWLSubObjectPropertyOfAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.ASYMMETRIC_OBJECT_PROPERTY)) {
        return getRelevantObjectPropertyExpressions((OWLAsymmetricObjectPropertyAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.SYMMETRIC_OBJECT_PROPERTY)) {
        return getRelevantObjectPropertyExpressions((OWLSymmetricObjectPropertyAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.SUB_PROPERTY_CHAIN_OF)) {
        return getRelevantObjectPropertyExpressions((OWLSubPropertyChainOfAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.TRANSITIVE_OBJECT_PROPERTY)) {
        return getRelevantObjectPropertyExpressions((OWLTransitiveObjectPropertyAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.INVERSE_OBJECT_PROPERTIES)) {
        return getRelevantObjectPropertyExpressions((OWLInverseObjectPropertiesAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES)) {
        return getRelevantObjectPropertyExpressions((OWLEquivalentObjectPropertiesAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.DISJOINT_OBJECT_PROPERTIES)) {
        return getRelevantObjectPropertyExpressions((OWLDisjointObjectPropertiesAxiom) axiom);
      } else {
        return Collections.emptySet();
      }
    }

    private Set<OWLDataPropertyExpression> getRelevantDataPropertyExpressions(
        OWLLogicalAxiom axiom) {
      if (axiom.isOfType(AxiomType.SUB_DATA_PROPERTY)) {
        return getRelevantDataPropertyExpressions((OWLSubDataPropertyOfAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.EQUIVALENT_DATA_PROPERTIES)) {
        return getRelevantDataPropertyExpressions((OWLEquivalentDataPropertiesAxiom) axiom);
      } else if (axiom.isOfType(AxiomType.DISJOINT_DATA_PROPERTIES)) {
        return getRelevantDataPropertyExpressions((OWLDisjointDataPropertiesAxiom) axiom);
      } else {
        return Collections.emptySet();
      }
    }

    private Set<OWLDatatype> getRelevantDatatypes(OWLLogicalAxiom axiom) {
      return getRelevantDatatypes((OWLDatatypeDefinitionAxiom) axiom);
    }

    private Set<OWLClassExpression> getRelevantClassExpressions(OWLSubClassOfAxiom axiom) {
      return Sets.newHashSet(axiom.getSubClass());
    }

    private Set<OWLClassExpression> getRelevantClassExpressions(
        OWLSubClassOfAxiomShortCut axiom) {
      return Sets.newHashSet(axiom.asOWLSubClassOfAxiom().getSubClass());
    }

    private Set<OWLClassExpression> getRelevantClassExpressions(OWLNaryClassAxiom axiom) {
      return axiom.classExpressions().collect(Collectors.toSet());
    }

    private Set<OWLClassExpression> getRelevantClassExpressions(OWLDisjointUnionAxiom axiom) {
      return getRelevantClassExpressions(axiom.getOWLDisjointClassesAxiom());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLSubObjectPropertyOfAxiom axiom) {
      return Sets.newHashSet(axiom.getSubProperty());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLAsymmetricObjectPropertyAxiom axiom) {
      return Sets.newHashSet(axiom.getProperty());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLSymmetricObjectPropertyAxiom axiom) {
      return Sets.newHashSet(axiom.getProperty());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLSubPropertyChainOfAxiom axiom) {
      return Sets.newHashSet(axiom.getPropertyChain());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLTransitiveObjectPropertyAxiom axiom) {
      return Sets.newHashSet(axiom.getProperty());
    }

    private Set<OWLObjectPropertyExpression> getRelevantObjectPropertyExpressions(
        OWLNaryPropertyAxiom<OWLObjectPropertyExpression> axiom) {
      return axiom.properties().collect(Collectors.toSet());
    }

    private Set<OWLDatatype> getRelevantDatatypes(OWLDatatypeDefinitionAxiom axiom) {
      return Sets.newHashSet(axiom.getDataRange().asOWLDatatype());
    }

    private Set<OWLDataPropertyExpression> getRelevantDataPropertyExpressions(
        OWLSubDataPropertyOfAxiom axiom) {
      return Sets.newHashSet(axiom.getSubProperty());
    }

    private Set<OWLDataPropertyExpression> getRelevantDataPropertyExpressions(
        OWLNaryPropertyAxiom<OWLDataPropertyExpression> axiom) {
      return axiom.properties().collect(Collectors.toSet());
    }

    private boolean compare(OWLClassExpression ce1, OWLClassExpression ce2) {
      OWLSubClassOfAxiom directTest = new OWLSubClassOfAxiomImpl(ce1, ce2, Sets.newHashSet());
      OWLSubClassOfAxiom reverseTest = new OWLSubClassOfAxiomImpl(ce2, ce1, Sets.newHashSet());

      boolean moreSpecific = reasoner.isEntailed(directTest);
      boolean lessSpecific = reasoner.isEntailed(reverseTest);

      if (moreSpecific) {
        if (lessSpecific) {
          return false;
        }
        return true;
      }
      return false;
    }

    private boolean compareClasses(Set<OWLClassExpression> ceSet1,
        Set<OWLClassExpression> ceSet2) {
      OWLClassExpression arg1 = ceSet1.iterator().next();
      OWLClassExpression arg2 = ceSet2.iterator().next();
      if (ceSet1.size() > 1) {
        arg1 = new OWLObjectIntersectionOfImpl(ceSet1);
      }
      if (ceSet2.size() > 1) {
        arg2 = new OWLObjectIntersectionOfImpl(ceSet2);
      }
      return compare(arg1, arg2);
    }

    private boolean compare(OWLObjectPropertyExpression pe1, OWLObjectPropertyExpression pe2) {
      OWLSubObjectPropertyOfAxiom directTest = new OWLSubObjectPropertyOfAxiomImpl(pe1, pe2,
          Sets.newHashSet());
      OWLSubObjectPropertyOfAxiom reverseTest = new OWLSubObjectPropertyOfAxiomImpl(pe2, pe1,
          Sets.newHashSet());

      boolean moreSpecific = reasoner.isEntailed(directTest);
      boolean lessSpecific = reasoner.isEntailed(reverseTest);

      return moreSpecific && !lessSpecific;
    }

    private boolean compareObjectProperties(Set<OWLObjectPropertyExpression> peSet1,
        Set<OWLObjectPropertyExpression> peSet2) {

      for (OWLObjectPropertyExpression next1 : peSet1) {
        for (OWLObjectPropertyExpression next2 : peSet2) {
          if (!compare(next1, next2)) {
            return false;
          }
        }
      }
      return true;
    }

    private boolean compareDatatypes(OWLLogicalAxiom axiom1, OWLLogicalAxiom axiom2) {
      return false;
    }

    private boolean compare(OWLDataPropertyExpression pe1, OWLDataPropertyExpression pe2) {
      OWLSubDataPropertyOfAxiomImpl directTest = new OWLSubDataPropertyOfAxiomImpl(pe1, pe2,
          Sets.newHashSet());
      OWLSubDataPropertyOfAxiomImpl reverseTest = new OWLSubDataPropertyOfAxiomImpl(pe2, pe1,
          Sets.newHashSet());

      boolean moreSpecific = reasoner.isEntailed(directTest);
      boolean lessSpecific = reasoner.isEntailed(reverseTest);

      return moreSpecific && !lessSpecific;
    }

    private boolean compareDataProperties(Set<OWLDataPropertyExpression> peSet1,
        Set<OWLDataPropertyExpression> peSet2) {

      for (OWLDataPropertyExpression next1 : peSet1) {
        for (OWLDataPropertyExpression next2 : peSet2) {
          if (!compare(next1, next2)) {
            return false;
          }
        }
      }
      return true;
    }
  }
}
