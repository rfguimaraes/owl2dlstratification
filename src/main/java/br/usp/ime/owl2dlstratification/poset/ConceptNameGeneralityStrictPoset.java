/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class ConceptNameGeneralityStrictPoset extends ConceptNameInducedStrictPoset {

  public ConceptNameGeneralityStrictPoset(OWLOntology ontology,
      OWLReasonerFactory reasonerFactory,
      OWLReasonerConfiguration reasonerConfiguration) {
    this(ontology, reasonerFactory.createReasoner(ontology, reasonerConfiguration));
  }

  public ConceptNameGeneralityStrictPoset(OWLOntology ontology, OWLReasoner reasoner) {
    super(ontology, ConceptNameGeneralityStrictPoset.conceptStratification(reasoner), reasoner);
  }

  static Map<OWLClass, Long> conceptStratification(OWLReasoner reasoner) {
    Map<OWLClass, Long> conceptStrata = new HashMap<>();

    if (reasoner.precomputableInferenceTypes().anyMatch(InferenceType.CLASS_HIERARCHY::equals)
        && !reasoner.isPrecomputed(InferenceType.CLASS_HIERARCHY)) {
      reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
    }

    Set<OWLClass> layer = reasoner.topClassNode().collect(Collectors.toSet());
    Set<OWLClass> nextLayer = new HashSet<>();

    long height = 0;

    while (!layer.isEmpty()) {
      for (OWLClass concept : layer) {
        conceptStrata.put(concept, height);
        NodeSet<OWLClass> subClasses = reasoner.getSubClasses(concept, true);
        subClasses.nodes().forEach(n -> n.entities().forEach(nextLayer::add));
      }

      // Fixed point
      if (layer.equals(nextLayer)) {
        break;
      }

      layer.clear();
      layer.addAll(nextLayer);
      nextLayer.clear();
      height++;
    }

    return conceptStrata;
  }

  @Override
  public Optional<Boolean> prec(OWLAxiom axiom, OWLAxiom otherAxiom) {
    if (!inDomain(axiom) || !inDomain(otherAxiom)) {
      return Optional.empty();
    }

    Set<OWLClass> minConcepts = getMinConcepts(axiom);
    Set<OWLClass> otherMinConcepts = getMinConcepts(otherAxiom);

    if (!Sets.intersection(minConcepts, otherMinConcepts).isEmpty()) {
      return Optional.of(false);
    }

    Optional<OWLClass> minConcept = minConcepts.stream().findAny();
    Optional<OWLClass> otherMinConcept = otherMinConcepts.stream().findAny();

    if (!minConcept.isPresent() || !otherMinConcept.isPresent()) {
      return Optional.empty();
    }

    Long minRank = conceptRanks.get(minConcept.get());
    Long otherMinRank = conceptRanks.get(otherMinConcept.get());

    if (minRank == null || otherMinRank == null) {
      return Optional.empty();
    }

    return Optional.of(minRank < otherMinRank);
  }
}
