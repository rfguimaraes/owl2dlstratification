/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import java.util.Optional;
import java.util.Set;

public interface StrictPoset<ItemType> {

  /* Returns true if e1 preceeds or is equal to e2 */
  Optional<Boolean> prec(ItemType e1, ItemType e2);

  /* Returns true if element in the poset's domain */
  boolean inDomain(ItemType element);

  /* Get minimal */
  Set<ItemType> getMinimal();

  /* Get successors of the item */
  Set<ItemType> getSuccessors(ItemType item);
}
