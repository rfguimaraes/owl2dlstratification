/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.strata;

import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import com.google.common.collect.MultimapBuilder.SetMultimapBuilder;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nonnull;

public class StrataFromStrictPoset<ItemType> implements Strata<ItemType> {

  protected final StrictPoset<ItemType> strictPoset;
  protected final Map<ItemType, Integer> ranks;
  protected final SetMultimap<Integer, ItemType> layers;

  public StrataFromStrictPoset(StrictPoset<ItemType> strictPoset) {
    this.strictPoset = strictPoset;
    ranks = new HashMap<ItemType, Integer>();
    layers = SetMultimapBuilder.treeKeys().hashSetValues().build();
    stratify();
  }

  private void stratify() {
    Integer rank = 0;

    Set<ItemType> currentLayer = Sets.newHashSet(strictPoset.getMinimal());
    Set<ItemType> nextLayer = Sets.newHashSet();

    while (!currentLayer.isEmpty()) {
      for (ItemType item : currentLayer) {
        Integer oldRank = ranks.get(item);
        if (oldRank != null) {
          layers.remove(oldRank, item);
        }
        ranks.put(item, rank);
        layers.put(rank, item);
        nextLayer.addAll(strictPoset.getSuccessors(item));
      }

      currentLayer.clear();
      currentLayer.addAll(nextLayer);
      nextLayer.clear();
      rank++;
    }
  }

  @Override
  public Optional<Integer> getRank(ItemType axiom) {
    return Optional.ofNullable(ranks.get(axiom));
  }

  @Override
  public Optional<Set<ItemType>> getLayer(int index) {
    if (index >= 0 && index < size()) {
      return Optional.ofNullable(layers.get(index));
    }
    return Optional.empty();
  }

  @Override
  public int size() {
    return layers.keySet().size();
  }

  /*@Override
  @Nonnull
  public Iterator<Set<ItemType>> iterator() {
    return new StrataIterator<ItemType>(this);
  }*/

}
