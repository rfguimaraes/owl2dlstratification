/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.strata;

import com.google.common.collect.AbstractIterator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nonnull;

/**
 * The interface Strata.
 *
 * @param <ItemType> the type parameter
 */
public interface Strata<ItemType> extends Iterable<Set<ItemType>> {

  /**
   * Gets the rank of the item in the strata
   *
   * @param item The queried item
   * @return An optional containing the rank if the axiom is in the strata, Optional.empty()
   * otherwise
   */
  Optional<Integer> getRank(ItemType item);

  /**
   * Gets the layer specified by the index.
   *
   * @param index the layer number
   * @return the specified layer or Optional.empty() if it does not exist
   */
  Optional<Set<ItemType>> getLayer(int index);

  /**
   * Returns how many layers the strata has. Warning: potentially breaks lazy
   * evaluation.
   * @return the number of layers
   */
  int size();

  @Override
  @Nonnull
  default Iterator<Set<ItemType>> iterator() {
    return new StrataIterator<>(this);
  }

  @Nonnull
  default Iterator<Set<ItemType>> reverseIterator() {
    return new StrataReverseIterator<>(this);
  }

  class StrataIterator<ItemType> extends AbstractIterator<Set<ItemType>> {

    private Strata<ItemType> strata;
    int current = 0;

    public StrataIterator(Strata<ItemType> strata) {
      this.strata = strata;

    }

    @Override
    protected Set<ItemType> computeNext() {
      Optional<Set<ItemType>> optLayer = strata.getLayer(current++);
      return optLayer.orElseGet(this::endOfData);
    }
  }
  class StrataReverseIterator<ItemType> extends AbstractIterator<Set<ItemType>> {

    private Strata<ItemType> strata;
    int current = 0;

    public StrataReverseIterator(Strata<ItemType> strata) {
      this.strata = strata;
      current = strata.size();
    }

    @Override
    protected Set<ItemType> computeNext() {
      Optional<Set<ItemType>> optLayer = strata.getLayer(--current);
      return optLayer.orElseGet(this::endOfData);
    }
  }
}
