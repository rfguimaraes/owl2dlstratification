/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import br.usp.ime.owl2dlstratification.experiment.StratificationMethod;
import br.usp.ime.owl2dlstratification.util.TestResourcesProvider;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;

public class AxiomSpecificityStrictPosetTest extends AxiomStrictPosetTest {

  @Override
  @ParameterizedTest
  @MethodSource("annotatedPathProvider")
  void respectsSerializedFile(Path path) throws IOException, URISyntaxException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());

    StrictPoset<OWLAxiom> axiomStrictPoset = null;
    try {
      axiomStrictPoset = new AxiomSpecificityStrictPoset(manager, ontology,
          new ReasonerFactory(), new SimpleConfiguration(600));
    } catch (OWLOntologyCreationException e) {
      e.printStackTrace();
    }

    respectsSerializedFile(axiomStrictPoset, ontology, path,
        StratificationMethod.AxS.toString());
  }
}
