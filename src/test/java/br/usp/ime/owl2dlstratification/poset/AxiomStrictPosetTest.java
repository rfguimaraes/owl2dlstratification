/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.poset;

import br.usp.ime.owl2dlstratification.util.AxiomStrictPosetMatcher;
import br.usp.ime.owl2dlstratification.util.TestResourcesProvider;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

public abstract class AxiomStrictPosetTest {


  public static List<Path> annotatedPathProvider() {
    return TestResourcesProvider.annotatedPathProvider();
  }

  @ParameterizedTest
  @MethodSource("annotatedPathProvider")
  abstract void respectsSerializedFile(Path path) throws IOException, URISyntaxException;

  protected void respectsSerializedFile(StrictPoset<OWLAxiom> axiomPartialOrder,
      OWLOntology ontology,
      Path path, String suffix) throws IOException, NullPointerException, URISyntaxException {
    URL partialOrderURLs = ClassLoader.getSystemClassLoader().getResource("partialorder");
    String dirName = Objects.requireNonNull(partialOrderURLs).toURI().getPath();
    String baseFilename = FilenameUtils.removeExtension(path.getFileName().toString());
    baseFilename = baseFilename + "_" + suffix + ".csv";
    AxiomStrictPosetMatcher
        .assertMatch(ontology, axiomPartialOrder, Paths.get(dirName, baseFilename));
  }
}
