/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owl.owlapi.OWLAnnotationImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLLiteralImplInteger;

public class OntologyAxiomIDAnnotator {

  public static OWLOntology annotated(OWLOntology ontology) throws OWLOntologyCreationException {
    OWLOntologyManager manager = ontology.getOWLOntologyManager();
    return annotated(manager, ontology);
  }

  public static OWLOntology annotated(OWLOntologyManager manager,
      OWLOntology ontology) throws OWLOntologyCreationException {
    OWLDataFactory dataFactory = manager.getOWLDataFactory();

    IRI originalIRI = ontology.getOntologyID().getOntologyIRI().orElse(IRI.generateDocumentIRI());
    IRI newIRI = IRI.create(originalIRI.toString() + "_axIDannotated");

    Iterator<OWLLogicalAxiom> axiomIterator = ontology.logicalAxioms().iterator();
    Set<OWLAxiom> annotatedAxioms = new HashSet<>();
    int axiomID = 0;

    OWLAnnotationProperty idAnnotation = dataFactory.getOWLAnnotationProperty(
        IRI.create(newIRI.toString(), "#AxiomID"));

    while (axiomIterator.hasNext()) {
      OWLLiteralImplInteger idValue = new OWLLiteralImplInteger(axiomID);
      OWLLogicalAxiom axiom = axiomIterator.next().getAxiomWithoutAnnotations();
      OWLLogicalAxiom annotatedAxiom = axiom.getAnnotatedAxiom(axiom.getClass(),
          Stream.of(new OWLAnnotationImpl(idAnnotation, idValue, Stream.of())));
      annotatedAxioms.add(annotatedAxiom);
      axiomID++;
    }
    OWLOntology annotatedOntology = manager.createOntology(newIRI);
    annotatedOntology.addAxioms(annotatedAxioms);
    return annotatedOntology;
  }
}
