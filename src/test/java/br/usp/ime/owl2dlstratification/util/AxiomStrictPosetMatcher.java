/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import static br.usp.ime.owl2dlstratification.util.AxiomIDAnnotatedOntology.getAxiomIDfromAnnotation;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

public class AxiomStrictPosetMatcher {

  public static boolean match(OWLOntology annotatedOntology,
      StrictPoset<OWLAxiom> axiomStrictPoset, Path csvPath)
      throws IOException {
    try (
        Reader reader = Files.newBufferedReader(csvPath);
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build()
    ) {
      String[] line;

      while ((line = csvReader.readNext()) != null) {
        if (!match(annotatedOntology, axiomStrictPoset, line[0], line[1], line[2])) {
          return false;
        }
      }
      return true;
    }
  }

  public static void assertMatch(OWLOntology annotatedOntology,
      StrictPoset<OWLAxiom> axiomStrictPoset, Path csvPath)
      throws IOException {
    try (
        Reader reader = Files.newBufferedReader(csvPath);
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build()
    ) {
      String[] line;

      while ((line = csvReader.readNext()) != null) {
        assertMatch(annotatedOntology, axiomStrictPoset, line[0], line[1], line[2]);
      }
    }
  }

  private static boolean match(OWLOntology annotatedOntology,
      StrictPoset<OWLAxiom> axiomStrictPoset, String axiomID1, String axiomID2,
      String result) {

    int axiomIntID1 = Integer.parseInt(axiomID1);
    int axiomIntID2 = Integer.parseInt(axiomID2);

    OWLLogicalAxiom axiom1 = annotatedOntology.logicalAxioms()
        .filter(axiom -> getAxiomIDfromAnnotation(annotatedOntology, axiom)
            .orElseThrow(NoSuchElementException::new) == axiomIntID1)
        .findAny().orElseThrow(NoSuchElementException::new);

    OWLLogicalAxiom axiom2 = annotatedOntology.logicalAxioms()
        .filter(axiom -> getAxiomIDfromAnnotation(annotatedOntology, axiom)
            .orElseThrow(NoSuchElementException::new) == axiomIntID2)
        .findAny().orElseThrow(NoSuchElementException::new);

    Optional<Boolean> optResult = axiomStrictPoset.prec(axiom1, axiom2);
    return optResult.filter(results -> results == Boolean.parseBoolean(result)).isPresent();
  }

  private static void assertMatch(OWLOntology annotatedOntology,
      StrictPoset<OWLAxiom> axiomStrictPoset, String axiomID1, String axiomID2,
      String result) {

    int axiomIntID1 = Integer.parseInt(axiomID1);
    int axiomIntID2 = Integer.parseInt(axiomID2);

    OWLLogicalAxiom axiom1 = annotatedOntology.logicalAxioms()
        .filter(axiom -> getAxiomIDfromAnnotation(annotatedOntology, axiom)
            .orElseThrow(NoSuchElementException::new) == axiomIntID1)
        .findAny().orElseThrow(NoSuchElementException::new);

    OWLLogicalAxiom axiom2 = annotatedOntology.logicalAxioms()
        .filter(axiom -> getAxiomIDfromAnnotation(annotatedOntology, axiom)
            .orElseThrow(NoSuchElementException::new) == axiomIntID2)
        .findAny().orElseThrow(NoSuchElementException::new);

    Optional<Boolean> optResult = axiomStrictPoset.prec(axiom1, axiom2);
    assertTrue(optResult.isPresent());
    String message =
        axiomID1 + ":" + axiom1.toString() + " x " + axiomID2 + ":" + axiom2.toString();
    assertEquals(Boolean.parseBoolean(result), optResult.get(), message);
  }
}
