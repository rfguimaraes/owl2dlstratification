/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import static br.usp.ime.owl2dlstratification.util.AxiomIDAnnotatedOntology.getAxiomIDfromAnnotation;

import br.usp.ime.owl2dlstratification.experiment.StratificationMethod;
import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import com.google.common.collect.Sets;
import com.opencsv.CSVWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import br.usp.ime.owl2dlstratification.experiment.StrataComputer;

public class AxiomStrictPosetWriter {

  public static void writeSingleCSV(OWLOntology annotatedOntology, Path outputPath)
      throws IOException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLReasonerFactory reasonerFactory = new ReasonerFactory();
    OWLReasonerConfiguration reasonerConfiguration = new SimpleConfiguration(600);

    StrataComputer computer = new StrataComputer(manager, annotatedOntology, reasonerFactory,
        reasonerConfiguration);
    SortedMap<StratificationMethod, StrictPoset<OWLAxiom>> result = new TreeMap<>(
        Comparator.comparing(Enum::name));
    for (StratificationMethod method : StratificationMethod.values()) {
      result.put(method,
          computer.getAxiomStrictPoset(method).orElseThrow(IllegalArgumentException::new));
    }

    Set<OWLLogicalAxiom> logicalAxioms = annotatedOntology.logicalAxioms()
        .collect(Collectors.toSet());

    try (
        Writer writer = Files.newBufferedWriter(outputPath);
        CSVWriter csvWriter = new CSVWriter(writer)
    ) {
      List<String> csvHeader = result.keySet().stream().map(Enum::name)
          .collect(Collectors.toList());

      csvHeader.add(0, "AxiomID2");
      csvHeader.add(0, "AxiomID1");

      String[] csvEntry = new String[csvHeader.size()];
      csvEntry = csvHeader.toArray(csvEntry);

      csvWriter.writeNext(csvEntry);

      for (Set<OWLLogicalAxiom> pair : Sets.combinations(logicalAxioms, 2)) {
        Iterator<OWLLogicalAxiom> iterator = pair.iterator();
        OWLLogicalAxiom first = iterator.next();
        OWLLogicalAxiom second = iterator.next();

        Optional<Integer> optID = getAxiomIDfromAnnotation(annotatedOntology, first);
        String firstAxiomID = String.valueOf(optID.orElse(null));

        optID = getAxiomIDfromAnnotation(annotatedOntology, second);
        String secondAxiomID = String.valueOf(optID.orElse(null));

        List<String> row = result.keySet().stream()
            .map(key -> result.get(key).prec(first, second))
            .map(opt -> (opt.isPresent()) ? String.valueOf(opt.get()) : "null")
            .collect(Collectors.toList());

        row.add(0, String.valueOf(secondAxiomID));
        row.add(0, String.valueOf(firstAxiomID));

        csvEntry = row.toArray(csvEntry);

        csvWriter.writeNext(csvEntry);
      }
    }
  }

  public static void write(OWLOntology annotatedOntology,
      StrictPoset<OWLAxiom> axiomStrictPoset, Path outputPath) throws IOException {
    Set<OWLLogicalAxiom> logicalAxioms = annotatedOntology.logicalAxioms()
        .collect(Collectors.toSet());

    try (
        Writer writer = Files.newBufferedWriter(outputPath);
        CSVWriter csvWriter = new CSVWriter(writer)
    ) {
      String[] header = {"AxiomID1", "AxiomID2", "Comparision"};

      csvWriter.writeNext(header);
      for (Set<OWLLogicalAxiom> pair : Sets.combinations(logicalAxioms, 2)) {
        Iterator<OWLLogicalAxiom> iterator = pair.iterator();
        OWLLogicalAxiom first = iterator.next();
        OWLLogicalAxiom second = iterator.next();

        Optional<Integer> optID = getAxiomIDfromAnnotation(annotatedOntology, first);
        String firstAxiomID = String.valueOf(optID.orElse(null));

        optID = getAxiomIDfromAnnotation(annotatedOntology, second);
        String secondAxiomID = String.valueOf(optID.orElse(null));

        Optional<Boolean> optResult = axiomStrictPoset.prec(first, second);
        optResult.ifPresent(results -> csvWriter
            .writeNext(new String[]{firstAxiomID, secondAxiomID, results.toString()}));
      }
    }
  }
}
