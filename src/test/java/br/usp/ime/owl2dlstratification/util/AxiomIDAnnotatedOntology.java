/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import java.util.NoSuchElementException;
import java.util.Optional;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;

public class AxiomIDAnnotatedOntology {

  public static Optional<Integer> getAxiomIDfromAnnotation(OWLOntology annotatedOntology,
      OWLAxiom axiom) {
    IRI ontologyIRI = annotatedOntology.getOntologyID().getOntologyIRI().orElseThrow(
        NoSuchElementException::new);
    OWLDataFactory dataFactory = OWLManager.getOWLDataFactory();
    OWLAnnotationProperty idAnnotation = dataFactory.getOWLAnnotationProperty(
        IRI.create(ontologyIRI.toString(), "#AxiomID"));
    Optional<OWLAnnotation> optAnnotation = axiom.annotations(idAnnotation).findAny();

    if (!optAnnotation.isPresent()) {
      return Optional.empty();
    }

    Optional<OWLLiteral> optLiteral = optAnnotation.get().getValue().asLiteral();
    return optLiteral.map(OWLLiteral::parseInteger);

  }
}
