/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringEscapeUtils;
import org.semanticweb.owlapi.model.OWLAxiom;
import uk.ac.manchester.cs.atomicdecomposition.Atom;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;

public class ADDotFileWriter {

  public static void writeDotFile(AtomicDecomposition atomicDecomposition, Path path) {
    String dotString = generateDotString(atomicDecomposition);
    try {
      Files.write(path, dotString.getBytes(),
          StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String generateDotString(AtomicDecomposition atomicDecomposition) {
    StringBuilder dotBuilder = new StringBuilder();
    dotBuilder.append("strict digraph {\n");

    for (Atom atom : atomicDecomposition.getAtoms()) {
      dotBuilder.append(atomToString(atom)).append(" -> {");
      for (Atom dependency : atomicDecomposition.getDependencies(atom, true)) {
        dotBuilder.append(atomToString(dependency)).append(" ");
      }
      dotBuilder.append("}\n");
    }

    dotBuilder.append("}\n");

    return dotBuilder.toString();
  }

  private static String atomToString(Atom atom) {
    return "\"" + StringEscapeUtils.escapeJava(
        atom.getAxioms().stream().map(OWLAxiom::toString).collect(Collectors.joining(",\n")))
        + "\"";
  }
}
