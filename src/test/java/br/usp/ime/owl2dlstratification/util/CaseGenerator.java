/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import br.usp.ime.owl2dlstratification.poset.ADDepthStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ADHeightStrictPoset;
import br.usp.ime.owl2dlstratification.poset.AxiomSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameGeneralityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import br.usp.ime.owl2dlstratification.experiment.StratificationMethod;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecompositionImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;

@Disabled
public class CaseGenerator {

  private static List<Path> pathProvider() {
    return TestResourcesProvider.pathProvider();
  }

  private static List<Path> annotatedPathProvider() {
    return TestResourcesProvider.annotatedPathProvider();
  }

  @ParameterizedTest
  @MethodSource("pathProvider")
  void annotateOntologies(Path path)
      throws FileNotFoundException, OWLOntologyStorageException, OWLOntologyCreationException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());

    String filename = FilenameUtils.removeExtension(path.getFileName().toString());
    filename = filename + "_annotated.owl.xml";
    String dirName = path.getParent().toString() + "/";
    OWLOntology annotatedOntology = OntologyAxiomIDAnnotator.annotated(manager, ontology);
    manager.saveOntology(annotatedOntology, new OWLXMLDocumentFormat(),
        new FileOutputStream(new File(dirName + filename)));
  }

  @ParameterizedTest
  @MethodSource("annotatedPathProvider")
  void createPartialOrderCSVs(Path path)
      throws OWLOntologyCreationException, IOException, NullPointerException, URISyntaxException {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());
    OWLReasonerFactory reasonerFactory = new ReasonerFactory();
    OWLReasonerConfiguration reasonerConfiguration = new SimpleConfiguration(600);

    ConceptNameSpecificityStrictPoset ciSpec = new ConceptNameSpecificityStrictPoset(
        ontology, reasonerFactory, reasonerConfiguration);
    ConceptNameGeneralityStrictPoset ciGen = new ConceptNameGeneralityStrictPoset(ontology,
        reasonerFactory, reasonerConfiguration);
    AxiomSpecificityStrictPoset axSpec = new AxiomSpecificityStrictPoset(manager, ontology,
        reasonerFactory, reasonerConfiguration);

    AtomicDecomposition botAD = new AtomicDecompositionImpl(ontology, ModuleType.BOT);
    AtomicDecomposition topAD = new AtomicDecompositionImpl(ontology, ModuleType.TOP);

    ADHeightStrictPoset buBotADHeight = new ADHeightStrictPoset(botAD);
    ADHeightStrictPoset buTopADHeight = new ADHeightStrictPoset(topAD);

    ADDepthStrictPoset tdBotADHeight = new ADDepthStrictPoset(botAD);
    ADDepthStrictPoset tdTopADHeight = new ADDepthStrictPoset(topAD);

    Map<StratificationMethod, StrictPoset<OWLAxiom>> partialOrderMap = new HashMap<>();

    partialOrderMap.put(StratificationMethod.CNS, ciSpec);
    partialOrderMap.put(StratificationMethod.CNG, ciGen);
    partialOrderMap.put(StratificationMethod.AxS, axSpec);
    partialOrderMap.put(StratificationMethod.BAH, buBotADHeight);
    partialOrderMap.put(StratificationMethod.TAH, buTopADHeight);
    partialOrderMap.put(StratificationMethod.BAD, tdBotADHeight);
    partialOrderMap.put(StratificationMethod.TAD, tdTopADHeight);
    URL partialOrderURLs = ClassLoader.getSystemClassLoader().getResource("partialorder");
    String dirName = Objects.requireNonNull(partialOrderURLs).toURI().getPath();
    String baseFilename = FilenameUtils.removeExtension(path.getFileName().toString());
    for (StratificationMethod method : partialOrderMap.keySet()) {
      String filename = baseFilename + "_" + method.toString() + ".csv";
      AxiomStrictPosetWriter
          .write(ontology, partialOrderMap.get(method), Paths.get(dirName, filename));
    }
    AxiomStrictPosetWriter
        .writeSingleCSV(ontology, Paths.get(dirName, baseFilename + "_all.csv"));
  }

  @ParameterizedTest
  @MethodSource("pathProvider")
  void writeDotFile(Path path) {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());

    String baseDir = "/home/ricardo/Desktop/tmp/dotfiles";
    String baseFilename = FilenameUtils.removeExtension(path.getFileName().toString());
    String botFilename = baseDir + "/" + baseFilename + "_bot.dot";
    String topFilename = baseDir + "/" + baseFilename + "_top.dot";

    ADDotFileWriter.writeDotFile(new AtomicDecompositionImpl(ontology, ModuleType.BOT),
        Paths.get(botFilename));
    ADDotFileWriter.writeDotFile(new AtomicDecompositionImpl(ontology, ModuleType.TOP),
        Paths.get(topFilename));
  }
}
