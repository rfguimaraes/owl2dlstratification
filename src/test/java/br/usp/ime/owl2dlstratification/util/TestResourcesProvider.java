/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.util;

import br.usp.ime.owl2dlstratification.poset.ADDepthStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ADHeightStrictPoset;
import br.usp.ime.owl2dlstratification.poset.AxiomSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameGeneralityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import com.google.common.io.Resources;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecompositionImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;

public class TestResourcesProvider {

  public static Stream<OWLOntology> ontologyProvider() {

    List<Path> result = pathProvider();

    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();

    return result.stream()
        .map(path -> loadOntology(manager, path.toString())).filter(Objects::nonNull);
  }

  public static List<Path> pathProvider() {
    List<Path> result = new ArrayList<>();
    String rootPath = Resources.getResource("ontologies").getPath();
    try {
      Files.walk(Paths.get(rootPath)).forEach(
          path -> {
            if (path.toString().endsWith(".owl")) {
              result.add(path);
            }
          }
      );

    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
    }

    /*result.clear();
    result.add(Paths.get("/home/ricardo/Desktop/try.xml.owl"));*/

    return result;
  }

  public static List<Path> annotatedPathProvider() {
    List<Path> result = new ArrayList<>();
    URL ontologiesURLs = ClassLoader.getSystemClassLoader().getResource("ontologies");
    try {
      URI uri = Objects.requireNonNull(ontologiesURLs).toURI();
      String convertedPath = uri.toString();
      URL guavaURL = Resources.getResource("ontologies");
      Files.walk(Paths.get(guavaURL.toURI())).forEach(
          path -> {
            if (path.toString().endsWith("_annotated.owl.xml")) {
              result.add(path);
            }
          }
      );

    } catch (IOException | NullPointerException | URISyntaxException e) {
      e.printStackTrace();
    }

    return result;
  }

  public static Stream<StrictPoset<OWLAxiom>> axiomStrictPosetProvider() {
    return annotatedPathProvider().stream()
        .flatMap(TestResourcesProvider::axiomStrictPosetStream);
  }

  public static Stream<Arguments> ontologyWithStrictPosetProvider() {
    return annotatedPathProvider().stream()
        .flatMap(TestResourcesProvider::ontologyWithStrictPosetStream);
  }

  private static Stream<Arguments> ontologyWithStrictPosetStream(Path path) {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());

    return axiomStrictPosetStream(path)
        .map(axiomStrictPoset -> Arguments.of(ontology, axiomStrictPoset));
  }

  private static Stream<StrictPoset<OWLAxiom>> axiomStrictPosetStream(Path path) {
    OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    OWLOntology ontology = TestResourcesProvider.loadOntology(manager, path.toString());
    OWLReasonerFactory reasonerFactory = new ReasonerFactory();
    OWLReasonerConfiguration reasonerConfiguration = new SimpleConfiguration(600);

    ConceptNameSpecificityStrictPoset ciSpec = new ConceptNameSpecificityStrictPoset(
        ontology, reasonerFactory, reasonerConfiguration);
    ConceptNameGeneralityStrictPoset ciGen = new ConceptNameGeneralityStrictPoset(ontology,
        reasonerFactory, reasonerConfiguration);
    AxiomSpecificityStrictPoset axSpec = null;
    try {
      axSpec = new AxiomSpecificityStrictPoset(manager, ontology,
          reasonerFactory, reasonerConfiguration);
    } catch (OWLOntologyCreationException e) {
      e.printStackTrace();
    }

    AtomicDecomposition botAD = new AtomicDecompositionImpl(ontology, ModuleType.BOT);
    AtomicDecomposition topAD = new AtomicDecompositionImpl(ontology, ModuleType.TOP);

    ADHeightStrictPoset buBotADHeight = new ADHeightStrictPoset(botAD);
    ADHeightStrictPoset buTopADHeight = new ADHeightStrictPoset(topAD);

    ADDepthStrictPoset tdBotADHeight = new ADDepthStrictPoset(botAD);
    ADDepthStrictPoset tdTopADHeight = new ADDepthStrictPoset(topAD);

    return Stream.of(ciSpec, ciGen, axSpec, buBotADHeight, buTopADHeight, tdBotADHeight,
        tdTopADHeight);
  }

  public static OWLOntology loadOntology(OWLOntologyManager manager, String filename) {
    OWLOntology result;
    try {
      InputStream owlFile = new FileInputStream(filename);
      result = manager.loadOntologyFromOntologyDocument(owlFile);
    } catch (OWLOntologyCreationException e) {
      result = null;
    } catch (Exception e) {
      e.printStackTrace();
      result = null;
    }
    return result;
  }

}
