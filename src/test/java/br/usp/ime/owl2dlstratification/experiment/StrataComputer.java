/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.experiment;

import br.usp.ime.owl2dlstratification.poset.ADDepthStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ADHeightStrictPoset;
import br.usp.ime.owl2dlstratification.poset.AxiomSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameGeneralityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.ConceptNameSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owl2dlstratification.strata.StrataFromStrictPoset;
import java.util.Optional;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class StrataComputer {

  private final StrataRequirements requirements;
  private final OWLOntologyManager manager;
  private final OWLOntology ontology;
  private final OWLReasonerFactory reasonerFactory;
  private final OWLReasonerConfiguration reasonerConfiguration;

  public StrataComputer(OWLOntologyManager manager, OWLOntology ontology,
      OWLReasonerFactory reasonerFactory,
      OWLReasonerConfiguration reasonerConfiguration) {
    this.requirements = new StrataRequirements(ontology, reasonerFactory, reasonerConfiguration);
    this.manager = manager;
    this.ontology = ontology;
    this.reasonerFactory = reasonerFactory;
    this.reasonerConfiguration = reasonerConfiguration;
  }

  public Optional<StrictPoset<OWLAxiom>> getAxiomStrictPoset(StratificationMethod method) {
    switch (method) {
      case BAH:
        return getBottomADHeightStrictPoset();
      case BAD:
        return getBottomADDepthStrictPoset();
      case TAH:
        return getTopADHeightStrictPoset();
      case TAD:
        return getTopADDepthStrictPoset();
      case CNS:
        return getConceptNameSpecificityStrictPoset();
      case CNG:
        return getConceptNameGeneralityStrictPoset();
      case AxS:
        return getAxiomSpecificityStrictPoset();
      default:
        return Optional.empty();
    }
  }

  public Optional<Strata<OWLAxiom>> getStrata(StratificationMethod method) {
    Optional<StrictPoset<OWLAxiom>> optAxiomStrictPoset = getAxiomStrictPoset(method);
    return optAxiomStrictPoset.map(StrataFromStrictPoset::new);
  }

  public Optional<StrictPoset<OWLAxiom>> getBottomADDepthStrictPoset() {
    return Optional.of(new ADDepthStrictPoset(requirements.getBotAD()));
  }

  public Optional<StrictPoset<OWLAxiom>> getBottomADHeightStrictPoset() {
    return Optional.of(new ADHeightStrictPoset(requirements.getBotAD()));
  }

  public Optional<StrictPoset<OWLAxiom>> getTopADDepthStrictPoset() {
    return Optional.of(new ADDepthStrictPoset(requirements.getTopAD()));
  }

  public Optional<StrictPoset<OWLAxiom>> getTopADHeightStrictPoset() {
    return Optional.of(new ADHeightStrictPoset(requirements.getTopAD()));
  }

  public Optional<StrictPoset<OWLAxiom>> getConceptNameSpecificityStrictPoset() {
    return Optional
        .of(new ConceptNameSpecificityStrictPoset(ontology, requirements.getReasoner()));
  }

  public Optional<StrictPoset<OWLAxiom>> getConceptNameGeneralityStrictPoset() {
    return Optional
        .of(new ConceptNameGeneralityStrictPoset(ontology, requirements.getReasoner()));
  }

  public Optional<StrictPoset<OWLAxiom>> getAxiomSpecificityStrictPoset() {

    StrictPoset<OWLAxiom> axiomStrictPoset = null;

    try {
      axiomStrictPoset = new AxiomSpecificityStrictPoset(manager, ontology, reasonerFactory,
          reasonerConfiguration);
    } catch (OWLOntologyCreationException e) {
      e.printStackTrace();
    }

    if (axiomStrictPoset != null) {
      return Optional.of(axiomStrictPoset);
    }
    return Optional.empty();
  }
}
