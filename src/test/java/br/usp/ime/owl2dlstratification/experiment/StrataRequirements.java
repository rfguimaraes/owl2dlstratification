/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.experiment;


import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecomposition;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecompositionImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;

public class StrataRequirements {

  private final OWLOntology ontology;
  private final OWLReasonerFactory reasonerFactory;
  private final OWLReasonerConfiguration reasonerConfiguration;
  private AtomicDecomposition botAD = null;
  private AtomicDecomposition topAD = null;
  private OWLReasoner reasoner = null;

  public StrataRequirements(OWLOntology ontology,
      OWLReasonerFactory reasonerFactory,
      OWLReasonerConfiguration reasonerConfiguration) {
    this.ontology = ontology;
    this.reasonerFactory = reasonerFactory;
    this.reasonerConfiguration = reasonerConfiguration;
  }

  AtomicDecomposition getBotAD() {
    if (this.botAD == null) {
      this.botAD = new AtomicDecompositionImpl(this.ontology, ModuleType.BOT);
    }
    return this.botAD;
  }

  AtomicDecomposition getTopAD() {
    if (this.topAD == null) {
      this.topAD = new AtomicDecompositionImpl(this.ontology, ModuleType.TOP);
    }
    return this.topAD;
  }

  OWLReasoner getReasoner() {
    if (this.reasoner == null) {
      this.reasoner = this.reasonerFactory.createReasoner(ontology, reasonerConfiguration);
    }
    return this.reasoner;
  }
}

