/*
 *    Copyright 2018-2020 OWL2DL-Stratification Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owl2dlstratification.strata;

import static org.junit.jupiter.api.Assertions.assertTrue;

import br.usp.ime.owl2dlstratification.poset.StrictPoset;
import br.usp.ime.owl2dlstratification.util.TestResourcesProvider;
import com.google.common.collect.Sets;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;

public class StrataFromStrictPosetTest {

  public static Stream<Arguments> ontologyWithPartialOrderProvider() {
    return TestResourcesProvider.ontologyWithStrictPosetProvider();
  }

  public static Stream<StrictPoset<OWLAxiom>> axiomPartialOrderProvider() {
    return TestResourcesProvider.axiomStrictPosetProvider();
  }

  @ParameterizedTest
  @MethodSource("ontologyWithPartialOrderProvider")
  public void stratificationIsContinuousFromZero(OWLOntology ontology,
      StrictPoset<OWLLogicalAxiom> axiomStrictPoset) {
    Strata<OWLLogicalAxiom> strata = new StrataFromStrictPoset<>(axiomStrictPoset);
    Integer maxRank = ontology.logicalAxioms().filter(axiomStrictPoset::inDomain)
        .map(axiom -> strata.getRank(axiom).orElseThrow(NoSuchElementException::new))
        .reduce(Math::max).orElseThrow(NoSuchElementException::new);

    for (long rank = 0; rank <= maxRank; rank++) {
      long finalRank = rank;
      assertTrue(ontology.logicalAxioms().filter(axiomStrictPoset::inDomain).anyMatch(
          axiom -> strata.getRank(axiom).orElseThrow(NoSuchElementException::new) == finalRank));
    }
  }

  @ParameterizedTest
  @MethodSource("axiomPartialOrderProvider")
  public void strataRespectsPartialOrder(StrictPoset<OWLLogicalAxiom> axiomStrictPoset) {

    Set<OWLLogicalAxiom> current = axiomStrictPoset.getMinimal();
    Set<OWLLogicalAxiom> allLogicalAxioms = new HashSet<>();

    while (!current.isEmpty()) {
      allLogicalAxioms.addAll(current);
      current = current.stream().flatMap(axiom -> axiomStrictPoset.getSuccessors(axiom).stream())
          .collect(Collectors.toSet());
    }

    for (Set<OWLLogicalAxiom> pair : Sets.combinations(allLogicalAxioms, 2)) {
      Strata<OWLLogicalAxiom> strata = new StrataFromStrictPoset<>(axiomStrictPoset);
      Iterator<OWLLogicalAxiom> iterator = pair.iterator();
      OWLLogicalAxiom first = iterator.next();
      OWLLogicalAxiom second = iterator.next();

      Integer firstRank = strata.getRank(first).orElseThrow(NoSuchElementException::new);
      Integer secondRank = strata.getRank(second).orElseThrow(NoSuchElementException::new);

      if (axiomStrictPoset.prec(first, second).orElse(false)) {

        //assertFalse(firstRank >= secondRank);
        if (firstRank >= secondRank) {
          axiomStrictPoset.prec(first, second);
          strata = new StrataFromStrictPoset<>(axiomStrictPoset);
        }
      }

      if (axiomStrictPoset.prec(second, first).orElse(false)) {
        //assertFalse(secondRank >= firstRank);
        if (secondRank >= firstRank) {
          axiomStrictPoset.prec(second, first);
          strata = new StrataFromStrictPoset<>(axiomStrictPoset);
        }
      }
    }
  }
}
