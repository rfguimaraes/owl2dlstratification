# OWL2DL-Stratification

Comparison among some stratification methods for OWL 2 DL ontologies (considering the fragment equivalent to the DL SROIQ).

## Building

We use Gradle to build the application:

1. Set up Gradle: https://gradle.org/install/ (we strongly recommend setting up gradle daemon too).
2. Clone this repository.
3. Open the root directory and type: ```./gradlew check```.
4. If everything is Ok, type: ```./gradlew test```. This should run the unit tests.
5. Run ```./gradlew build``` to build the library.
